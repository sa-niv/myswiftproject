//
//  SwiftOnGitLabTests.swift
//  SwiftOnGitLabTests
//
//  Created by niv ben-porath on 10/05/2022.
//

import XCTest
@testable import SwiftOnGitLab

class SwiftOnGitLabTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        let vm = MyViewModel()
        let firstNumber = 3
        let secondNumber = 5
        let result = vm.add(firstNumber, to: secondNumber)
        XCTAssertEqual(result, 8, "only test in the world")
    }
}
