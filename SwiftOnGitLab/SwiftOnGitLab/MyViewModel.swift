//
//  MyViewModel.swift
//  SwiftOnGitLab
//
//  Created by niv ben-porath on 10/05/2022.
//

import Foundation

class MyViewModel {
    func add(_ firstNumber: Int, to secondNumber: Int) -> Int {
        return firstNumber + secondNumber
    }
}
